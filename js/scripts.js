var menuButton = $('#nav-icon1');
var arrowUpButton = $('.arrowUp');
var mobimenu = $('.topstripe__list');

$(document).ready(function(){
	//activate mobile menu
	$(menuButton).click(function(){
		$(this).toggleClass('open').toggleClass('fixedmenu');
	});
	//scroll to element
	$('a[href^="#"]').on('click', function(event) {
    var target = $(this.getAttribute('href'));
    if( target.length ) {
        event.preventDefault();
        $('html, body').stop().animate({
            scrollTop: target.offset().top
        }, 1000);
		}
	});
	//arrow scroll up
	arrowUpButton.on('click', function() {
        $("html, body").animate({ scrollTop: 0 }, "slow");
        return false;
    });
	 $(window).scroll(function () {
        if ($(this).scrollTop() > 100) {
            arrowUpButton.fadeIn();
        } else {
            arrowUpButton.fadeOut();
        }
	 });
     //Show mobi menu
     $(menuButton).click(function(){

        if((menuButton).hasClass("open")){
            mobimenu.toggleClass('active');
        } else {
            mobimenu.removeClass('active').toggleClass('fixedmenu');
        }
     });
     $(".slidesbig > div:gt(0)").hide();
        setInterval(function() { 
        $('.slidesbig > div:first')
            .fadeOut(3000)
            .next()
            .fadeIn(3000)
            .end()
            .appendTo('.slidesbig');
        },  4000);
});




